# REQUIRED parameters

# The ID of the Limbo dataset
LIMBO_DATASET_ID=train_2
# The ID of the Limbo dataset
LIMBO_DATASET_NAME="Bahnhoefe RNI"
LIMBO_DATASET_LICENSE=NullLicense
# The distribution in the mcloud catalog that the Limbo dataset is based on
BASE_IRI=distribution-5670eb707309b5e1523b0d97fb0353ac
VERSION=0.3-SNAPSHOT


